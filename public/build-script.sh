#!/bin/bash

# NOTE: Set "export DEVELOPMENT=1" to download the git repositories with the local public key so that the repositories are editable by default.

# config
GITLAB_HOST_PREFIX=https://
GITLAB_HOST_SUFFIX=gitlab.com
GITLAB_HOST=${GITLAB_HOST_PREFIX}${GITLAB_HOST_SUFFIX}
SPLINTER_FOLDER=.splinter

# functions
function check_tool(){
    if [ "$(which $1)" == "" ]; then
        echo Please install $1 and make it accessible on PATH. >&2
        exit 1
    fi
}

function get_http(){
    URL="$1"
    OUTPUT="$2"

    if [ ! "$(which curl)" == "" ]; then
        curl "$URL" -L -s -o "$OUTPUT"
    elif [ ! "$(which wget)" == "" ]; then
        wget "$URL" -q -O "$OUTPUT" 
    else
        echo No download tool detected. Please install curl or wget. >&2
        exit 1
    fi
}
function get_artifact() {
    TOKEN=$1
    FOLDER=$2
    PROJECT=$3
    BRANCH=$4
    JOBNAME=$5
    if [ "$JOBNAME" == "" ]; then
        echo usage: get_artifact TOKEN FOLDER PROJECT BRANCH JOBNAME
        exit 1
    fi
    check_tool unzip
    rm -Rf $FOLDER &> /dev/null
    mkdir $FOLDER &> /dev/null
    pushd $FOLDER &> /dev/null
    if [ ! "$(which curl)" == "" ]; then
        curl $GITLAB_HOST/api/v4/projects/$PROJECT/jobs/artifacts/$BRANCH/download?job=$JOBNAME -L -H "PRIVATE-TOKEN: $TOKEN" -s -o artifact.zip
    elif [ ! "$(which wget)" == "" ]; then
        wget $GITLAB_HOST/api/v4/projects/$PROJECT/jobs/artifacts/$BRANCH/download?job=$JOBNAME --header="PRIVATE-TOKEN: $TOKEN" -O artifact.zip --continue -q
    fi

    unzip -qq artifact.zip && rm -f artifact.zip
    popd &> /dev/null
}
function get_git() {
    TOKEN=$1
    FOLDER=$2
    PROJECT=$3
    BRANCH=$4
    if [ "$BRANCH" == "" ]; then
        echo usage: get_git TOKEN FOLDER PROJECT BRANCH
        exit 1
    fi
    check_tool git
    if [ ! -d ${FOLDER} ]; then
        if [ "$DEVELOPMENT" == "1" ]; then
            git clone --depth=1 --branch=${BRANCH} git@${GITLAB_HOST_SUFFIX}:${PROJECT}.git ${FOLDER}
        else
            git clone --depth=1 --single-branch --branch=${BRANCH} ${GITLAB_HOST_PREFIX}oauth2:$TOKEN@${GITLAB_HOST_SUFFIX}/${PROJECT}.git ${FOLDER}
        fi
    else
            pushd $FOLDER &> /dev/null
            git pull
            popd &> /dev/null
    fi
}

# argument handler
if [ "$1" == "splinter" ]; then
    if [ ! -f ${SPLINTER_FOLDER}/splinter.sh ]; then
        "$0" get-splinter
    fi
    shift
    ${SPLINTER_FOLDER}/splinter.sh "${@}" || exit $?
fi
if [ "$1" == "get-splinter" ]; then
    TOKEN=`"$0" token-splinter`
    if [ "$DEVELOPMENT" == "1" ]; then
        get_git $TOKEN ${SPLINTER_FOLDER} splinter-build/splinter master
    else
        if [ "$OS" == "Windows_NT" ]; then
            get_artifact $TOKEN ${SPLINTER_FOLDER} splinter-build%2Fsplinter master build-windows
        else
            get_artifact $TOKEN ${SPLINTER_FOLDER} splinter-build%2Fsplinter master build-linux
        fi
    fi
fi
if [ "$1" == "get-winsdk" ]; then
    TOKEN=`"$0" token-splinter`
    get_artifact $TOKEN cpp_winsdk splinter-build%2Fcompilers%2Fcppwindowssdk master build-x64
fi
if [ "$1" == "get-cpp-reflektor" ]; then
    TOKEN=`"$0" token-splinter`
    get_git $TOKEN cppreflektor splinter-build/compilers/cppreflektor master
fi
if [ "$1" == "get-artifact" ]; then
    shift
    get_artifact "${@}"
fi
if [ "$1" == "get-git" ]; then
    shift
    get_git "${@}"
fi
if [ "$1" == "token-cfc" ]; then
    get_http http://cfcgroup.gitlab.io/read_token.txt "-"
fi
if [ "$1" == "token-splinter" ]; then
    get_http http://splinter-build.gitlab.io/read_token.txt "-"
fi
